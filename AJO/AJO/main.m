//
//  main.m
//  AJO
//
//  Created by Samreen Noor on 26/08/2016.
//  Copyright © 2016 Apple Txlabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
