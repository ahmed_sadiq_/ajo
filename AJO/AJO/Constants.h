//
//  Constants.h
//  AJO
//
//  Created by Samreen Noor on 26/08/2016.
//  Copyright © 2016 Apple Txlabz. All rights reserved.
//

#ifndef Constants_h
#define Constants_h
#pragma mark-
#pragma mark Screen Sizes
#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667)
#define IS_IPHONE_5 ([[UIScreen mainScreen] bounds].size.height == 568)
#define IS_IPHONE_5S ([[UIScreen mainScreen] bounds].size.height == 568)

#define IS_IPHONE_4 ([[UIScreen mainScreen] bounds].size.height == 480)
#define IS_IPAD ([[UIScreen mainScreen] bounds].size.height == 1024)
#define IS_IPHONE_6Plus ([[UIScreen mainScreen] bounds].size.height == 736)


#endif /* Constants_h */
