//
//  LoginViewController.m
//  AJO
//
//  Created by Samreen Noor on 26/08/2016.
//  Copyright © 2016 Apple Txlabz. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
{
    UITextField *currentField;

}
@end

@implementation LoginViewController

-(void) setUpView{
    if (IS_IPHONE_6) {
        
        _btnHeightConstrain.constant = 65;
        _boxViewHeightConstrain.constant = 300.0;
        _lblNameTopConstrain.constant = 36;
    }
    else if (IS_IPHONE_6Plus){
        _btnHeightConstrain.constant = 70;
        _boxViewHeightConstrain.constant = 320.0;
        _lblNameTopConstrain.constant = 40;
        _boxViewBottomConstrain.constant = 15;
        
    }
    


}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [singleTap setNumberOfTapsRequired:1];
    // singleTap.delegate=delegate;
    [self.view addGestureRecognizer:singleTap];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - texfield delegates

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    currentField=textField;
    if ([_tfUserName isEqual:textField]) {
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:0
                         animations:^{
                             
                             
                             _boxViewBottomConstrain.constant=50;
                             
                             [self.view layoutIfNeeded];
                             
                         }
                         completion:^(BOOL finished) {
                             
                         }];
        

    }
    else{
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:0
                         animations:^{
                             
                             
                             _boxViewBottomConstrain.constant=100;
                             
                             [self.view layoutIfNeeded];
                             
                         }
                         completion:^(BOOL finished) {
                             
                         }];

    }
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{

}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    if ([textField isEqual:_tfUserName]) {
        
        [_tfPassword becomeFirstResponder];
    }
    else{
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:0
                         animations:^{
                             
                             
                             _boxViewBottomConstrain.constant=7;// move view without scroll view
                             [textField resignFirstResponder];

                             [self.view layoutIfNeeded];
                             
                         }
                         completion:^(BOOL finished) {
                             
                         }];
        

    }
    
    return YES;
}

-(void)dismissKeyboard {
    
    NSLog(@"screen touch");
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:0
                         animations:^{
    
                            
                             _boxViewBottomConstrain.constant=7;// move view without scroll view
                             [currentField resignFirstResponder];

                             [self.view layoutIfNeeded];
    
                         }
                         completion:^(BOOL finished) {
    
                         }];
    
  

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
