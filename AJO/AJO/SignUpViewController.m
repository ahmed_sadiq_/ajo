//
//  SignUpViewController.m
//  AJO
//
//  Created by Samreen Noor on 26/08/2016.
//  Copyright © 2016 Apple Txlabz. All rights reserved.
//

#import "SignUpViewController.h"
#import "Constants.h"
@interface SignUpViewController ()

@end

@implementation SignUpViewController
-(void) setUpView{
    
    [_imgProfile.layer setCornerRadius:50.0];
    [_imgProfile.layer setMasksToBounds:YES];
    if (IS_IPHONE_6Plus) {
        _btnHeightConstrain.constant = 380;
        _btnHeightConstrain.constant = 70;
        _boxViewTopConstrain.constant=99;
        _tfHeightConstrain.constant= 44;
        _tfBottomConstrain.constant = 60;
        _tfBottomSpace.constant =20;
        _imgViewTopConstrain.constant=30;
        
    }
    else if (IS_IPHONE_6){
        _btnHeightConstrain.constant = 365;
        _btnHeightConstrain.constant = 65;
        _boxViewTopConstrain.constant=77;
        _tfHeightConstrain.constant= 44;
        _tfBottomConstrain.constant = 50;
        _tfBottomSpace.constant =20;
        _imgViewTopConstrain.constant=10;
        _boxViewBottomConstrain.constant=18;
    
    }
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - IBAction
- (IBAction)btnBackLogin:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
