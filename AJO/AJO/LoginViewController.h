//
//  LoginViewController.h
//  AJO
//
//  Created by Samreen Noor on 26/08/2016.
//  Copyright © 2016 Apple Txlabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
@interface LoginViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boxViewHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblNameTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boxViewBottomConstrain;
@property (weak, nonatomic) IBOutlet UIImageView *bgImgView;
@property (weak, nonatomic) IBOutlet UITextField *tfUserName;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;

@end
