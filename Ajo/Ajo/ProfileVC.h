//
//  ProfileVC.h
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileVC : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *profilePic;
@property (strong, nonatomic) IBOutlet UITextField *nameTxt;
@property (strong, nonatomic) IBOutlet UITextField *emailTxt;
- (IBAction)cameraBtnPressed:(id)sender;
- (IBAction)galleryBtnPressed:(id)sender;

- (IBAction)savePressed:(id)sender;
@end
