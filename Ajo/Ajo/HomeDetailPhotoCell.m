//
//  HomeDetailPhotoCell.m
//  Ajo
//
//  Created by Ahmed Sadiq on 16/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "HomeDetailPhotoCell.h"

@implementation HomeDetailPhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
