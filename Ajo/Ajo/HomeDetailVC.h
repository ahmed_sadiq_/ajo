//
//  HomeDetailVC.h
//  Ajo
//
//  Created by Ahmed Sadiq on 15/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"

@interface HomeDetailVC : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *postDataSource;
@property (strong, nonatomic) NSMutableArray *filteredArray;
@property (strong, nonatomic) IBOutlet UITableView *mainTblView;
@property (strong, nonatomic) IBOutlet ASStarRatingView *staticStarRatingView;
- (IBAction)backPressed:(id)sender;

- (IBAction)tab1Pressed:(id)sender;
- (IBAction)tab2Pressed:(id)sender;
- (IBAction)tab3Pressed:(id)sender;
@end
