//
//  SignUpVC.h
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeVC.h"

@interface SignUpVC : UIViewController<UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (strong, nonatomic) HomeVC *viewController;
@property (strong, nonatomic) IBOutlet UITextField *nameTxt;
@property (strong, nonatomic) IBOutlet UITextField *emailTxt;
@property (strong, nonatomic) IBOutlet UIImageView *profileImg;
@property (strong, nonatomic) IBOutlet UITextField *userNameTxt;
@property ( strong , nonatomic ) UITabBarController *tabBarController;
@property ( strong , nonatomic ) UINavigationController *navController;

- (IBAction)cameraBtnPressed:(id)sender;
- (IBAction)galleryBtnPressed:(id)sender;

- (IBAction)signUpPressed:(id)sender;

- (IBAction)loginPressed:(id)sender;
- (IBAction)forgotPswdPressed:(id)sender;

@end
