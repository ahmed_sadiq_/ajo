//
//  ProfileVC.m
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ProfileVC.h"
#import "UIImageView+RoundImage.h"

@interface ProfileVC ()

@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_profilePic roundImageCorner];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cameraBtnPressed:(id)sender {
}

- (IBAction)galleryBtnPressed:(id)sender {
}

- (IBAction)savePressed:(id)sender {
}
@end
