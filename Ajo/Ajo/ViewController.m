//
//  ViewController.m
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ViewController.h"
#import "SignUpVC.h"
#import "RecommendationVC.h"
#import "ProfileVC.h"
#import "SettingsVC.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)crateAccountPressed:(id)sender {
    SignUpVC *signupController = [[SignUpVC alloc] initWithNibName:@"SignUpVC" bundle:nil];
    [self.navigationController pushViewController:signupController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}

- (IBAction)forgotPasswordPressed:(id)sender {
}

- (IBAction)loginPressed:(id)sender {
    [self createTabBarAndControl];
}


-(void)createTabBarAndControl {
    
    self.viewController = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    
    self.navController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    [self.navController.tabBarItem setSelectedImage:[[UIImage imageNamed:@"discover.png"]
                                                     imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    
    [self.navController.tabBarItem setImage:[[UIImage imageNamed:@"discover.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    self.navController.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    [self.navController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *recommendationVC;
    recommendationVC = [[RecommendationVC alloc] initWithNibName:@"RecommendationVC" bundle:[NSBundle mainBundle]];
    
    
    UINavigationController *recommendationNavController = [[UINavigationController alloc] initWithRootViewController:recommendationVC];
    
    
    [recommendationVC.tabBarItem setSelectedImage:[[UIImage imageNamed:@"recommendation.png"]
                                            imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    [recommendationVC.tabBarItem setImage:[[UIImage imageNamed:@"recommendation.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    
    recommendationVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    [recommendationVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *profileVC;
    profileVC = [[ProfileVC alloc] initWithNibName:@"ProfileVC" bundle:[NSBundle mainBundle]];
    UINavigationController *profileNavController = [[UINavigationController alloc] initWithRootViewController:profileVC];
    
    [profileVC.tabBarItem setSelectedImage:[[UIImage imageNamed:@"profile_tab.png"]
                                          imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    [profileVC.tabBarItem setImage:[[UIImage imageNamed:@"profile_tab.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    profileVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    [profileVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *settingVC;
    settingVC = [[SettingsVC alloc] initWithNibName:@"SettingsVC" bundle:[NSBundle mainBundle]];
    UINavigationController *settingNavController = [[UINavigationController alloc] initWithRootViewController:settingVC];
    
    [settingVC.tabBarItem setSelectedImage:[[UIImage imageNamed:@"settings.png"]
                                            imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    [settingVC.tabBarItem setImage:[[UIImage imageNamed:@"settings.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    settingVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    [settingVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.tabBarController = [[UITabBarController alloc] init] ;
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:self.navController, recommendationNavController,profileNavController,settingNavController,nil];
    
    [self.tabBarController.tabBar setBackgroundColor:[UIColor whiteColor]];
    
    self.viewController.navigationController.navigationBar.tintColor = [UIColor blackColor];
    //self.viewController.navigationController.navigationBar
    [[[UIApplication sharedApplication]delegate] window].rootViewController = self.tabBarController;
}


#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:nil up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:nil up:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
@end
