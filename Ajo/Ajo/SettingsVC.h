//
//  SettingsVC.h
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *cityTxt;
@property (strong, nonatomic) IBOutlet UITextField *stateTxt;
@property (strong, nonatomic) IBOutlet UITextField *countryTxt;
@property (strong, nonatomic) IBOutlet UITextField *zipTxt;
@property (strong, nonatomic) IBOutlet UITextField *distanceTxt;

- (IBAction)savePressed:(id)sender;
@end
