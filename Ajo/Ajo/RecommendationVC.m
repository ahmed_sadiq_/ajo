//
//  RecommendationVC.m
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "RecommendationVC.h"
#import "HomeCell.h"
#import "HomeDetailVC.h"
#import <QuartzCore/QuartzCore.h>

@interface RecommendationVC ()

@end

@implementation RecommendationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCell * cell = (HomeCell *)[_mainTblView dequeueReusableCellWithIdentifier:@"HomeCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"HomeCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    
    cell.leftView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.leftView.layer.borderWidth = 0.8f;
    cell.leftView.layer.cornerRadius = 6;
    cell.leftView.layer.masksToBounds = YES;
    cell.leftImg.image = [UIImage imageNamed:@"pool.PNG"];
    
    cell.rightView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.rightView.layer.borderWidth = 0.8f;
    cell.rightView.layer.cornerRadius = 6;
    cell.rightView.layer.masksToBounds = YES;
    cell.rightImg.image = [UIImage imageNamed:@"hotels.png"];
    
    
    [cell.leftBtn addTarget:self action:@selector(leftViewBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.rightBtn addTarget:self action:@selector(rightViewBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 220;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
}


#pragma mark - Cell Utility Methods

- (void) leftViewBtnPressed : (id) sender {
    
    HomeDetailVC *homeDetailController = [[HomeDetailVC alloc] initWithNibName:@"HomeDetailVC" bundle:nil];
    homeDetailController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:homeDetailController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    
}

- (void) rightViewBtnPressed : (id) sender {
    
    HomeDetailVC *homeDetailController = [[HomeDetailVC alloc] initWithNibName:@"HomeDetailVC" bundle:nil];
    homeDetailController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:homeDetailController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
}

@end
