//
//  HomeDetailVC.m
//  Ajo
//
//  Created by Ahmed Sadiq on 15/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "HomeDetailVC.h"
#import "HomeDetailReviewCell.h"
#import "HomeDetailPostCell.h"
#import "PostModel.h"
#import "HomeDetailPhotoCell.h"
#import "UIImageView+RoundImage.h"

@interface HomeDetailVC ()

@end

@implementation HomeDetailVC
@synthesize staticStarRatingView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    staticStarRatingView.canEdit = YES;
    staticStarRatingView.maxRating = 5;
    staticStarRatingView.rating = 2.5;
    
    [self fillDummyData];
}

- (void) fillDummyData {
    _postDataSource = [[NSMutableArray alloc] init];
    _filteredArray = [[NSMutableArray alloc] init];
    for(int i=0; i<10; i++) {
        
        PostModel *pModel = [[PostModel alloc] init];
        
        
        if(i%3 == 0) {
            pModel.postTyper = 1;
            
        }
        else if(i%3 == 1) {
            pModel.postTyper = 2;
            
        }
        else {
            pModel.postTyper = 3;
            
        }
        
        [_filteredArray addObject:pModel];
        [_postDataSource addObject:pModel];
    }
    [_mainTblView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)tab1Pressed:(id)sender {
    [_filteredArray removeAllObjects];
    for(int i=0; i<_postDataSource.count; i++) {
        
        PostModel *pModel = [_postDataSource objectAtIndex:i];
        
        
        if(pModel.postTyper == 1 ) {
            [_filteredArray addObject:pModel];
            
        }
    }
    [_mainTblView reloadData];
}

- (IBAction)tab2Pressed:(id)sender {
    [_filteredArray removeAllObjects];
    for(int i=0; i<_postDataSource.count; i++) {
        
        PostModel *pModel = [_postDataSource objectAtIndex:i];
        
        
        if(pModel.postTyper == 2) {
            [_filteredArray addObject:pModel];
            
        }
    }
    [_mainTblView reloadData];
}

- (IBAction)tab3Pressed:(id)sender {
    [_filteredArray removeAllObjects];
    for(int i=0; i<_postDataSource.count; i++) {
        
        PostModel *pModel = [_postDataSource objectAtIndex:i];
        
        
        if(pModel.postTyper == 3) {
            [_filteredArray addObject:pModel];
            
        }
    }
    [_mainTblView reloadData];
}


#pragma mark - Table View Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _filteredArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PostModel *pModel = [_filteredArray objectAtIndex:indexPath.row];
    if(pModel.postTyper == 1 ) {
        HomeDetailReviewCell * cell = (HomeDetailReviewCell *)[_mainTblView dequeueReusableCellWithIdentifier:@"HomeDetailReviewCell"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"HomeDetailReviewCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        
        [cell.profileImg roundImageCorner];
        
        cell.staticRatingView.canEdit = NO;
        cell.staticRatingView.maxRating = 5;
        cell.staticRatingView.rating = indexPath.row;
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
    else if(pModel.postTyper == 2 ) {
        HomeDetailPostCell * cell = (HomeDetailPostCell *)[_mainTblView dequeueReusableCellWithIdentifier:@"HomeDetailPostCell"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"HomeDetailPostCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        
        [cell.profileImg roundImageCorner];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
    else {
        HomeDetailPhotoCell * cell = (HomeDetailPhotoCell *)[_mainTblView dequeueReusableCellWithIdentifier:@"HomeDetailPhotoCell"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"HomeDetailPhotoCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        
        [cell.profileImg roundImageCorner];
        cell.mainScroller.contentSize = CGSizeMake(600, 30);
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PostModel *pModel = [_filteredArray objectAtIndex:indexPath.row];
    
    
    if(pModel.postTyper == 3 ) {
        return 280;
    }
    else {
        return 100;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
@end
