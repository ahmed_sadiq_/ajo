//
//  RecommendationVC.h
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecommendationVC : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITableView *mainTblView;

@end
