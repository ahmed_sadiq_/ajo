//
//  PostModel.h
//  Ajo
//
//  Created by Ahmed Sadiq on 16/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    review = 1,
    post,
    photo
} PostType;

@interface PostModel : NSObject
@property (strong, nonatomic) NSString *postHolderName;
@property (strong, nonatomic) NSString *postTime;
@property (strong, nonatomic) NSString *postDescription;
@property float rating;
@property int postTyper;
@property (assign, nonatomic) PostType postType;





@end
